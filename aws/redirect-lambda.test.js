const lambda = require('./redirect-lambda').handler;
const callback = (e,r) => r

test('no exception thrown with AWS sample event', () => {
    const event = require('./viewer-request-test-event.json');
    lambda(event, null, callback)
});

test('top level uri ending in /index.html is not changed', () => {
    testUnchangedUri("https://woo.com/index.html");
});

test('top level uri ending in random file is not changed', () => {
    testUnchangedUri("https://woo.com/random.file");
});

test.skip('top level uri is redirected to index.html', () => {
    testWithUri("https://woo.com", "https://woo.com/index.html");
});

test('top level uri with trailing slash is redirected to index.html', () => {
    testWithUri("https://woo.com/", "https://woo.com/index.html");
});

test('subdirectory with trailing slash is redirected to index.html', () => {
    testWithUri("https://woo.com/sub/", "https://woo.com/sub/index.html");
});

test('subdirectory ending in /index.html is not changed', () => {
    testUnchangedUri("https://woo.com/sub/index.html");
});

test('subdirectory ending in random file is not changed', () => {
    testUnchangedUri("https://woo.com/sub/random.file");
});

test('several subdirectories are redirected to an index.html', () => {
    testWithUri("https://woo.com/sub/dirs/", "https://woo.com/sub/dirs/index.html");
});

test('several subdirectories ending in /index.html is not changed', () => {
    testUnchangedUri("https://woo.com/sub/dirs/index.html");
});

test('several subdirectories ending in random file is not changed', () => {
    testUnchangedUri("https://woo.com/sub/dirs/random.file");
});

function testUnchangedUri(uri) {
    testWithUri(uri, uri);
}

function testWithUri(uri, expectedResult) {
    const event = eventWithRequestUri(uri);
    expect(lambda(event, null, callback)).toHaveProperty("uri", expectedResult);
}

function eventWithRequestUri(uri) {
    var event = JSON.parse(JSON.stringify(require('./viewer-request-test-event.json')));
    event.Records[0].cf.request.uri = uri;
    return event;
}