This is the source repository for https://eddarmitage.com. It's a site built
using [Hugo][hugo], and published to [Amazon S3][aws-s3] using GitLab CI.
Hugo is a widely-used static site generator, so this repository contains the
markdown "source", and it produces html that is served to all users, rather
than being generated dynamically for each user, which is why it's able to be
served using Amazon S3 - AWS's blob storage offerring that can be configured to
server web pages publicly.

# Theme
The theme I've used was originally based on the [Texture][texture-theme] theme
for Jekyll sites by Samarjeet Singh, although I've already customised it a
reasonable amount and had to port it to Hugo. Overtime, I hope to end up with a
completely customised site of my own design.

# Generating Locally
To generate a site preview locally using a local installation of the
[Hugo cli tool][hugo-cli] execute the following:

```bash
$ hugo server
```

This will generate the page in-memory, and server the site on
http://localhost:1313

## Testing pipelines
To build using the GitLab CI pipeline, use the following command:

```bash
$ gitlab-runner exec docker build
```

[hugo]: https://gohugo.io
[hugo-cli]: https://gohugo.io/getting-started/installing/
[aws-s3]: https://aws.amazon.com/s3/
[texture-theme]: https://github.com/thelehhman/texture