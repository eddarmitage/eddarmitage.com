+++
date = "2019-12-17T21:00:00Z"
tags = ["life", "projects"]
title = "Finishing Things"
+++
I often find myself starting small projects, only to hit some kind of problem,
my interest to wane, and progress to eventually grind to a halt; leaving it
incomplete. This year I've really wanted to start getting things finished, or
at least published in whatever state they happen to be, in the hope that it
would motivate me to complete them.
<!--more-->
# Projects
There were a number of projects that I wanted to complete this
year:

1. Complete a tiny python CLI tool I'd written for importing photos into a
    structured hierarchy based on their EXIF data.
2. Publish a Java library I'd written several years ago (for a previous plan to
    develop this site no less) that generates URL-friendly slugs from Strings.
3. Get a personal site online. I've wanted somewhere to share things for a long
    time, and the idea of a statically generated site serverd from AWS
    particularly appealed.

Let's review the state of each of these in turn:

## photo-import tool
[This](https://gitlab.com/eddarmitage/photo-import) is a really tiny cli
command I'd written to help my photo management process. Given a directory full
of photos, the tool moves the photos into another directory structure,
arranging them into a hierarchy by the date taken from the EXIF metadata.

The project was developed as it was to include a number of "interesting"
techniques and technology choices, that I either don't get to do much in my
day-to-day work, or that are becoming an increasingly important part of my
company's work, including:

* [GitLab CI](https://docs.gitlab.com/ee/ci/) - I didn't want to have to be
    manually building such a small project, that long-term I'll hopefully be
    changing infrequently, and GitLab CI is a continuous integration solution
    that is used increasingly at [work](https://www.ocadotechnology.com).
* [Semantic Release](https://github.com/semantic-release/semantic-release) - I
    liked the idea of using structured commit messages, and also not having to
    decide what the current version of the application was. For a command
    that's installed on user's machines, I think [calver](http://calver.org)
    could also be interesting, but for this project I chose
    [semver](https://semver.org).
* BDD (Or "Natual Language" scenario testing) - I've been aware of tools like
    [Cucumber](http://cucumber.io) for a good long while now, but it's not
    something I get to use at my job (our scenario testing framework uses tests
    written in standard Java). Using [behave](http://behave.readthedocs.io), I
    get to write tests in Gherkin, and maintain a human-readable collection of
    documents describing the current functionality.

I may still have [plenty of features](https://gitlab.com/eddarmitage/photo-import/issues)
that I want to add to this tool, but I've really enjoyed exploring the Python
ecosystem, and I'm proud of managed to at least get this far enough to get the
beta [published on pypi](https://pypi.org/project/photo-import/).

## Slugger library
Originally, I was planning on writing my own Java-based static site generator
with a web-accessible UI. A small part of that application was
[this](http://github.com/eddarmitage/Slugger/) library, that produced the
URL-friendly slugs needed. I've never really produced a library before, and I
wanted to keep this to core Java (at work, whilst we don't use Spring or
anything like that, we do use [Guava](https://github.com/google/guava/) fairly
extensively). This was also the first time I got to experiment with (an
at-the-time beta of) [JUnit 5](https://junit.org/junit5/), and again, I used
[Cucumber JVM](https://github.com/cucumber/cucumber-jvm) for BDD-style testing.

The basic functionality of this library is complete, but I really wanted to get
it published on Maven central, and have proper CI set up for it. Getting onto
Maven central is the current stumbling block (it's a little more involved than
I'd expected, especially if I don't want to have to manually promote each
release into the public repository), but I'm still really hoping to get this
completed by the end of the year. The CI is mostly incomplete because I'm
using an unfamiliar tool ([Travis](http://travis-ci.org) - chosen because this
project is hosted on GitHub rather than GitLab, and also because I wanted to be
able to compare a few different CI tools) and I think I've been a little bit
over-ambitious (building and testing with a huge matrix of runtime versions)
for my initial attempt - I'd still like to add some of this in later, but for
now I think it's best to just get _something_ working. Again, this feels
achievable before the end of the year.

## This site
And so the final thing that I want to finish this year is this site. As
mentioned above, I'd originally wanted to write my own static-site builder, but
I think that was too big a task in reality (It also wasn't that interesting a 
project - a lot of it was gluing together existing libraries for parsing
markdown and applying templating, and I kept finding myself getting distracted
writing optimisations into caches, when my test-site only contained three or
four posts... 🙄).

Eventually, I admitted defeat, and this was really the start of my realisation
that just getting _something_ finished was more important than getting
_something great_ started.

Using Jekyll isn't necessarily what I wanted to be doing, and whilst I don't
have any real desire to learn about Ruby, it is at least a popular and
widely-used tool, and I'm hoping to still be able to publish to Amazon S3 via
some form of automated pipeline.

# Summary
With any luck, I'll have all of these projects completed by the end of the
year, even if they're not quite how I originally envisaged.
