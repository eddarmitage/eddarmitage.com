+++
date = "2020-01-13T18:00:00Z"
tags = ["projects"]
title = "Here we go again"
+++
Having [previously explained]({{< relref "finishing-things.md#this-site" >}})
how I've built this site using [Jekyll](https://jekyllrb.com) and GitLab CI,
I've gone and ripped out the entirety of the static site generation, and
replaced Jekyll with [Hugo](https://gohugo.io).
<!--more-->

There were a number of reasons for this change, some of which I was aware of
whilst creating the Jekyll site, and some that only became apparent when
looking into how to make future changes to the site that I was initially
ignoring to allow me to get _something_ live.

Looking at the [list of issues closed off by the migration][migration-mr], some
of them are clearly because I didn't know what I was doing with the initial
Jekyll skeleton, resulting in a lot of near-duplicates in the config. And some
of my annoyances were pretty minor personal preferences (such as all of the
Jekyll directory names being prefixed with `_underscores/`.

Reading about other people who have made a similar migration, the common
concern seems to be about performance when building a large site. This isn't
something I imagine will be a problem for the forseeable future, but the thing
that does appeal as the amount of content grows is the fact that Hugo keeps
_all_ of the site content within a directory, keeping the repository cleaner as
the only files in the root folder now are for CI or configuration.

This also gives me the opportunity to work a bit in some new ecosystems, with
[Go Templates](https://golang.org/pkg/text/template/) and config and
front-matter specified using [TOML](https://github.com/toml-lang/toml).

Perhaps this just shows the avantage of getting that first bit of momentum, and
actually completing the first iteration, encouraging continued change. I don't
think I would have developed this site nearly as much if I hadn't even
completed the first version.

[migration-mr]: https://gitlab.com/eddarmitage/eddarmitage.com/merge_requests/9