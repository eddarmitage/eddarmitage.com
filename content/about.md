---
layout: page
title: About
permalink: /about/
---

I'm Edd and I'm a software engineer based in London, England. By day I work at
Ocado Technology on Java control systems to
[plan and control a fleet of robots][infoq-cornford]. Away from the office, I
dabble with projects using Python, Java and other technologies.

[infoq-cornford]: https://www.infoq.com/articles/java-robot-swarms/